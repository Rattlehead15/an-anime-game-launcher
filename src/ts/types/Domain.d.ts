type DomainInfo = {
    uri: string;
    remoteIp?: string;
    localIp?: string;
    available: boolean;
};

export type { DomainInfo };
